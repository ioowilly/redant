define([
    'jquery',
    'lodash',
    'backbone',
    'text!templates/modules/sendMessage.html'
], function($, _, Backbone, template) {
    var SendMessageView = Backbone.View.extend({
        el: '.content',
        events : {
            'submit' : 'renderResults',
        },  
        renderResults: function(e){
            e.preventDefault();
            $('#result').html("You sent '"+ $('#messageText').val() + "' to this view");
        },
        render: function() {

            $(this.el).html(template);
            
        }
    });

    return SendMessageView;
});
