define([
    'lodash',
    'backbone',
    'jquery',
    'text!templates/modules/showStores.html',
    'models/stores'
], function(_, Backbone, $, template, Stores) {
    var stores = new Stores();

    return Backbone.View.extend({
        el: '.content',
        events : {'submit' : 'renderResults'},

        renderResults: function(e){
            e && e.preventDefault();
            $lon = $('#longitude'),
            $loader = $('#showStoresLoader').show(),
            $lat = $('#latitude');
            
            // clear previous results
            $('#stores').empty();
            $('#message').text('');
            stores.search($lat.val(), $lon.val()).then(function(stores){
                $('#stores').append(stores.length && stores.map(function(store){
                   return '<tr><td>'+store.name+'</td><td>'+store.address+'</td><td>'+store.phone+'</td></tr>';
                }).join('') || '<tr><td>No stores found in this area</td></tr>');
            }, function(){
                $('#message').text('Oops, something went wrong');
            }).always(function(){
                $loader.hide();
            });
        },

        init: function(){
            var defer = $.Deferred();
            $lon = $('#longitude'),
            $loader = $('#showStoresLoader'),
            $lat = $('#latitude');

            if(navigator && navigator.geolocation){
                navigator.geolocation.getCurrentPosition(
                    function (position) {
                        $loader.hide();
                    
                        $lon.val(position.coords.longitude);
                        $lat.val(position.coords.latitude);
                        defer.resolve();
                    },
                    function(){
                        $loader.hide();
                        defer.resolve();
                    }
                );
            } else {
               $loader.hide();
               defer.resolve();
            }
            return defer.promise();
        },
        render: function () {
            $(this.el).html(template);
            this.init().then(this.renderResults);
        }
    });
});
