define([
    'lodash',
    'backbone',
    'jquery',
    'text!templates/modules/daysOfWeek.html'
], function( _, Backbone, $, template) {
    var day;
    var daysOfTheWeek = ['monday','tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    var nextSevenDays = [];

    var DaysOfWeekView = Backbone.View.extend({
        el: '.content',

        initialise: function()
        {
            // SPA may be open for a long time without refreshing the page. To support the edge case when the day actually changed since the last refresh, we check the current day against the original day
            if(day !== new Date().getDay()){
                day = new Date().getDay();
                nextSevenDays = daysOfTheWeek.slice(day, daysOfTheWeek.length).concat(daysOfTheWeek.slice(0, day)).join(', ');
            }            
        },

        nextSevenDays : function () {            
            //Task 3 - Complete this function efficiently
            return nextSevenDays;
        },

        render: function () {
            this.initialise();
            $(this.el).html(template);
            $('#days').html(this.nextSevenDays());
        }
    });

    return DaysOfWeekView;
});
