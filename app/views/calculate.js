define([
    'lodash',
    'backbone',
    'jquery',
    'text!templates/modules/calculate.html'
], function(_, Backbone, $, template) {
    var CalculateView = Backbone.View.extend({
        el: '.content',
        events : {
            'submit' : 'renderResults',
            'click #sendButton': 'renderResults'
        },
        dividend : 5,
        
        calculate : function (dividend)
        {
            return this.dividend / parseFloat($('#inputNumber').val());
        },
        
        renderResults: function (e) {
            e.preventDefault();
            var result = this.calculate(this.dividend);
            $('#result').html(isFinite(result) && 'The answer is ' + result || 'Please enter a valid number (other than 0)');
        },
        
        render: function () {

            $(this.el).html(template);
            
            $('.dividend-value').html(this.dividend);
        }
    });

    return CalculateView;
});
