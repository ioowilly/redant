define([
  'jquery',
  'backbone'
], function($, Backbone) {
  var url = 'http://cloudservices.arcadiagroup.co.uk/storestock/storestock';

  return Backbone.Model.extend({
    search: function(lat, long){
      return $.ajax({
        url: url,
        jsonp: 'jsonp_callback',
        dataType: 'jsonp',
        timeout: 1000, // workaround for jsonp error handling within jquery, regular error callbacks are not fired
        data: {
          brand: 12556,
          lat: lat,
          dist: 50,
          long: long,
          res: 5
        }
      })
      .then(function(response){
        return response.stores && response.stores.count > 0 ? response.stores.store : [];
      })
      .then(function(stores){
        return stores.map(function(store){
          return {
            name: store.storeName + ' | ' + store.brandName,
            address: store.address1 + ', ' + store.address2 + ', ' + store.address3 + ', ' + store.address4 + ', ' + store.postcode,
            phone: store.telephoneNumber
          }
        });
      });
    }
  });
});
